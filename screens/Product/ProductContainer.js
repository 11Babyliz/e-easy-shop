import { useEffect, useState } from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";

const data = require("../../assets/data/Product.json");

import ProductList from "./ProductList";
function ProductContainer(){
    const [product, setProducts] = useState();
useEffect(() => {
    setProducts(data);
    return () => {
        setProducts([]);
    }
}, []);

    return ( 
    <View style={styles.container}>
        <Text>Product Container</Text>
        <View  style={styles.listContainer}>
            <FlatList 
                numColumns={2}
                data={product} 
                renderItem={({ item }) => <ProductList product = {item}/>} 
                keyExtractor={(item) => item._id.$oid}
            />
        </View>      
    </View>
    );
}


export default ProductContainer;


const styles = StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        backgroundColor: 'gainsboro',
    },
    listContainer: {
        width: '100%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        backgroundColor: 'gainsboro',
    },
})